<?php

use Illuminate\Database\Seeder;
use Automotriz\Aseguradora;

class AseguradorasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Aseguradora::class,55)->create();
    }
}
